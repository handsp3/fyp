﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor.Animations;

public class AnimationController : MonoBehaviour
{
    //Scene Objects
    public Animator character;
    public GameObject animationPanel;
    public GameObject buttonPrefab;
    //public GameObject animator;
    public Button playButton;

    //Animations list
    private AnimationClip[] animations;
    
    //Animator Variables
    private AnimatorController controller;
    private AnimatorStateMachine rootStateMachine;
    private AnimatorState idleState;

    //Current trigger to activate
    private string currentStateTrigger;

    // Start is called before the first frame update
    void Start()
    {
        //Create initial animation controller
        controller = AnimatorController.CreateAnimatorControllerAtPath("Assets/Animations/CharacterAnimationController.controller");
        rootStateMachine = controller.layers[0].stateMachine;
        idleState = rootStateMachine.AddState("idle");

        character.runtimeAnimatorController = controller;

        //Load all available animations as both clips and animator components.
        animations = Resources.LoadAll<AnimationClip>("Animations");
        Animator[] animators = Resources.LoadAll<Animator>("Animations");

        //Create a button for each animation
        for (int i = 0; i < animations.Length; i++)
        {
            AnimationClip animation = animations[i];
            string name = animators[i].name;

            createButton(animation, name);
            addState(animation, name);
        }

        //Listener for the playbutton
        playButton.onClick.AddListener(() =>
        {
            character.SetTrigger(currentStateTrigger);
        });
    }

    // Update is called once per frame
    void Update()
    {
        //Move the character to the centre once the animator is in idle state.
        if (character.GetCurrentAnimatorStateInfo(0).IsName("idle") && character.transform.position != Vector3.zero)
        {
            character.transform.position = Vector3.zero;
        }
    }

    /**
     * Create a button for an animation, given the animation and it's name.
     */
    public void createButton(AnimationClip animation, string name)
    {
        //Instantiate button
        GameObject newButton = Instantiate(buttonPrefab);
        Button button = newButton.GetComponent<Button>();
        newButton.transform.SetParent(animationPanel.transform);
        
        //Set text
        Text text = newButton.GetComponentInChildren<Text>();
        text.text = name;

        //Add onclick
        button.onClick.AddListener(() => {
            AnimatorControllerParameter[] parameters = controller.parameters;
            Debug.Log(name);
            currentStateTrigger = getTriggerName(name);
        });
    }

    /** 
     * Adds a state to the animator's state machine using the given animation clip.
     */
    public void addState(AnimationClip animation, string name)
    {
        AnimatorState state = rootStateMachine.AddState(name);

        controller.AddParameter(getTriggerName(name), AnimatorControllerParameterType.Trigger);

        state.motion = animation;

        //Adding transitions
        AnimatorStateTransition stateToIdle = state.AddTransition(idleState);
        stateToIdle.hasExitTime = true;

        AnimatorStateTransition idleToState = idleState.AddTransition(state);
        idleToState.AddCondition(AnimatorConditionMode.If, 1, getTriggerName(name));
        idleToState.duration = 0;

    }

    /**
     * Update the animator used, should be called whenever the character model is changed by the characterModelController.
     */
    public void updateCharacter(Animator newCharacter)
    {
        character = newCharacter;
        character.runtimeAnimatorController = controller;
    }

    /**
     * Return the name of the transition trigger given the name of the animation. 
     */
    private string getTriggerName(string name)
    {
        return name.Replace(" ", "") + "Active";
    }
}
