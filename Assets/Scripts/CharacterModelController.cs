﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class CharacterModelController : MonoBehaviour
{
    public GameObject character;
    public GameObject characterPanel;
    public GameObject buttonPrefab;
    public AnimationController animationController;

    private GameObject[] charSkins;


    // Start is called before the first frame update
    void Start()
    {   
        //Create a button for every character model available.
        charSkins = Resources.LoadAll<GameObject>("Char");
        foreach(GameObject skin in charSkins)
        {
            createButton(skin);
        }

        //Adjust position of panel.
        RectTransform panelTransform = characterPanel.GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void createButton(GameObject skin)
    {
        //Instantiate button
        GameObject newButton = Instantiate(buttonPrefab);
        Button button = newButton.GetComponent<Button>();
        newButton.transform.SetParent(characterPanel.transform);
        //Set text
        Text text = newButton.GetComponentInChildren<Text>();
        text.text = skin.name;
        //Add onclick
        button.onClick.AddListener(() => changeModel(skin));
    }
    
    /**
     * Changes the character model to the given game object (model). Called from buttons in the character menu.
     */
    public void changeModel(GameObject skin)
    {
        Destroy(character.transform.GetChild(0).gameObject);
        GameObject newCharModel = Instantiate(skin) as GameObject;
        newCharModel.transform.SetParent(character.transform);
        animationController.updateCharacter(newCharModel.GetComponent<Animator>());
    }
}
